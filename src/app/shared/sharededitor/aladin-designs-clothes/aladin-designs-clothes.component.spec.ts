import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AladinDesignsClothesComponent } from './aladin-designs-clothes.component';

describe('AladinDesignsClothesComponent', () => {
  let component: AladinDesignsClothesComponent;
  let fixture: ComponentFixture<AladinDesignsClothesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AladinDesignsClothesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AladinDesignsClothesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
