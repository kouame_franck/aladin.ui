import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePartnerComponent } from './home-partner/home-partner.component';
import { RegisterPartnerComponent } from './register-partner/register-partner.component';
import { ForgotpwdComponent } from './forgotpwd/forgotpwd.component';
import { LoginPartnerComponent } from './login-partner/login-partner.component';
import { DashpartnerComponent } from './dashpartner/dashpartner.component';
import { PartnerGuard} from '../core';
const routes: Routes = [
  {path:'',component:HomePartnerComponent},
  {path:'register',component:RegisterPartnerComponent},
  {path:'login-partner',component:LoginPartnerComponent},
  {path:'forgotpwd',canActivate:[PartnerGuard],component:ForgotpwdComponent},
  {path:'dashboard',canActivate:[PartnerGuard],component:DashpartnerComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PartnerRoutingModule { }
