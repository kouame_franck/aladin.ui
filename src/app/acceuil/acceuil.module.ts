import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import{SharedModule} from  '../shared';
import { HomeComponent } from './home/home.component';
import { AcceuilRoutingModule } from './acceuil-routing.module';
import { DevmodeComponent } from './devmode/devmode.component';
@NgModule({
  declarations: [
    HomeComponent,
    DevmodeComponent
  ],
  imports: [
    AcceuilRoutingModule,
    SharedModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class AcceuilModule { }
