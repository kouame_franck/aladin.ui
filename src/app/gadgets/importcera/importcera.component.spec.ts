import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportceraComponent } from './importcera.component';

describe('ImportceraComponent', () => {
  let component: ImportceraComponent;
  let fixture: ComponentFixture<ImportceraComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImportceraComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
