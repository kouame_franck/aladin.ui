import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AladinProductsComponent } from './aladin-products.component';

describe('AladinProductsComponent', () => {
  let component: AladinProductsComponent;
  let fixture: ComponentFixture<AladinProductsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AladinProductsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AladinProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
