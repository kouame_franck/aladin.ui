import { Component, OnInit } from '@angular/core';
import { ListService } from 'src/app/core';
@Component({
  selector: 'app-listgadget',
  templateUrl: './listgadget.component.html',
  styleUrls: ['./listgadget.component.scss']
})
export class ListgadgetComponent implements OnInit {
  gadgets:any;
  url="/editor/gadgets/"
  hideimport=false
  hidegadget=true
  imagepreview:any
  showchoices=false;
  shw=true
  text="Je veux imprimer ma créa !!"
  isstylo=false
  istasse=false
   isportcle=false
  constructor(private l :ListService) { }

  ngOnInit(): void {
    this.l.getGadgets().subscribe(
      res=>{
        this.gadgets=res;
      }
      ,
      err=>{
        console.log(err);
      }
    )
  }
  View(){
    let view = document.getElementById('view');
    view?.scrollIntoView({behavior:"smooth"})
   }

   Upload(event:any){
    let file =event.target.files[0]
    const reader = new FileReader();
     reader.onload = () => {
  
   this.imagepreview = reader.result;
   
   };
   
   reader.readAsDataURL(file);
   this.showimportcrea()
    console.log(file)
 
  }
  showimportcrea(){
    this.hidegadget=false
    this.hideimport=true
  }

  displaychoices(){
    this.showchoices=true;
  }
  OnclickTasse(){
    if(this.istasse==false){
      this.text="J'importe mon visuel de tasse";
      this.showchoices=true;
      this.istasse=true;
      this.isstylo=false
      this.isportcle=false
    }else{
      this.showchoices=true;
      this.text="Je veux imprimer ma créa!!";
  
    }
   }
   OnclickStylo(){
    if(this.istasse==false){
      this.text="J'importe mon visuel de Stylo";
      this.showchoices=true;
      this.istasse=false;
      this.isstylo=true
      this.isportcle=false
    }else{
      this.showchoices=true;
      this.text="Je veux imprimer ma créa!!";
  
    }
   }
   OnclickPorte(){
    if(this.istasse==false){
      this.text="J'importe mon visuel de Porte cle";
      this.showchoices=true;
      this.istasse=false;
      this.isstylo=false
      this.isportcle=true
    }else{
      this.showchoices=true;
      this.text="Je veux imprimer ma créa!!";
  
    }
   }
   
letchange(value:boolean){
  this.hidegadget=value
  this.hideimport=!value
}
}
