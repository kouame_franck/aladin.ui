import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PwdforgotComponent } from './pwdforgot.component';

describe('PwdforgotComponent', () => {
  let component: PwdforgotComponent;
  let fixture: ComponentFixture<PwdforgotComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PwdforgotComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PwdforgotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
