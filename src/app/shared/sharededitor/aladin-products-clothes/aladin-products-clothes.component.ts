import { Component, OnInit,Input, Output,EventEmitter } from '@angular/core';
import { ListService } from 'src/app/core';
@Component({
  selector: 'app-aladin-products-clothes',
  templateUrl: './aladin-products-clothes.component.html',
  styleUrls: ['./aladin-products-clothes.component.scss']
})
export class AladinProductsClothesComponent implements OnInit {
  cltobj:any;
  prints:any;
  gadgets:any;
  packs:any;
  disps:any;
  @Input() id:any;
  @Input() name:any
  cloth="cloth";
  print="prints"
  pack="packs";
  gadget="gadgets";
  disp="disps";
  current=1;
  current_disp=1
  current_gad=1;
  current_print=1;
  current_pack:any=1;
  pages=[1,2,3,4,5];
  loading=false;

  @Output() newItemEvent =new EventEmitter<any>();

  constructor(private p: ListService) { }

  ngOnInit(): void {
    this.current=this.pages[0]
    this.p.getclt(this.current).subscribe(
      res=>{
    this.cltobj=res;
    this.cltobj= this.cltobj.data
    },
    err=>{
      console.log(err);
    }
    );
  }


  currentpage(event:any){
    event.className="page-item "
    console.log(event)
    let id=event.target.id
    this.current=id
    console.log(event.target);
    if(this.loading){
      this.onloading()

    }
    this.onloading()
    this.p.getclt(id).subscribe(
      res=>{
        let data=res;
        if(data.data.length>0){
          this.onloading()
          this.cltobj=res;
          this.cltobj= this.cltobj.data
          console.log(res)
        }
        
        else{
          this.onloading()
    
        }
        
    },
    err=>{
     this.onloading()
    })
  }

 nextPage(){
 this.current=(+this.current)+1;
 if(this.loading){
  this.onloading()

}
this.onloading()
 this.p.getclt(this.current).subscribe(
  res=>{
    let data=res;
    if(data.data.length>0){
      this.onloading()
      this.cltobj=res;
      this.cltobj= this.cltobj.data;
      console.log(res);
    }else{
      this.onloading()

    }
    
},
err=>{
 this.onloading()
}
)

}

previouspage(){
  if(+this.current>1&&this.current!=undefined){
  this.current=(+this.current)-1;
  if(this.loading){
    this.onloading()

  }
  this.onloading()
 this.p.getclt(this.current).subscribe(
  res=>{
    let data=res;
    if(data.data.length>0){
      this.onloading()
      this.cltobj=res;
      this.cltobj= this.cltobj.data
      console.log(res)
    }
    
    else{
      this.onloading()

    }
    
},
err=>{
 this.onloading()
}

)
  }
}

addNewItem(value: any) {
  //custimg
  console.log(value)
  let data={
    url:value.lib_img,
    name:value.name_cloths,
    price:value.price,
    type:JSON.parse(value.type_imp),
    back:value.lib_back,
    size:JSON.parse(value.size),
    user:value.staff,
    category:"clothes"
  }
  console.log(data)
 this.newItemEvent.emit(data);
}

onloading(){
  this.loading=!this.loading;
}
}
