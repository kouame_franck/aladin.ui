import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AladinDesignsComponent } from './aladin-designs.component';

describe('AladinDesignsComponent', () => {
  let component: AladinDesignsComponent;
  let fixture: ComponentFixture<AladinDesignsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AladinDesignsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AladinDesignsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
